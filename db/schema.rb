# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181224130042) do

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.string "first_name"
    t.string "last_name"
    t.integer "sum_learned_words"
    t.integer "percent_right_word_per_day"
    t.integer "percent_right_word_per_last_day"
    t.integer "percent_right_word_per_penultimate_day"
    t.integer "percent_right_word_per_day_before_3day_ago"
    t.integer "percent_right_word_per_day_before_4day_ago"
    t.integer "percent_right_word_per_day_before_5day_ago"
    t.integer "percent_right_word_per_day_before_6day_ago"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
