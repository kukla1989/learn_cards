class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :email 
      t.string :password_digest
      t.string :first_name
      t.string :last_name
      t.integer :sum_learned_words   
      t.integer :percent_right_word_per_day  #this and below column is percen right typed words per day
      t.integer :percent_right_word_per_last_day
      t.integer :percent_right_word_per_penultimate_day
      t.integer :percent_right_word_per_day_before_3day_ago
      t.integer :percent_right_word_per_day_before_4day_ago
      t.integer :percent_right_word_per_day_before_5day_ago
      t.integer :percent_right_word_per_day_before_6day_ago

      t.timestamps
    end
   add_index :users, :email, unique: true
  end
end
