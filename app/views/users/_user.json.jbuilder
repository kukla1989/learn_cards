json.extract! user, :id, :email, :first_name, :last_name, :word_level, :word_repeat, :created_at, :updated_at
json.url user_url(user, format: :json)
